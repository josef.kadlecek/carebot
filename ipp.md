# Intellectual Property Protection

Discuss the technical and non-technical measures you would implement to prevent unauthorized copying and use of a Docker deployment in an on-premise environment. Focus on how you would safeguard the application within the boundaries of the client's infrastructure.

## Introduction

Personally, I dont think the problem is actually solvable.  If there were a solution, we would not be able to violate IP (pirate software) at all. And ~~we~~ some popeple are cairtainly capable to pirate software of companies which invest tremendous efforts towards making it ~~impossible~~ hard. 


## Non technicall meassures

The contract should include the licence for usage of the software, which forbids the distribution and "rents" the software only as managed service.

Proprietary licences surely forbids reverse engeneering and NDAs signed with the people who have access to the servers are surely the easiest way to go. 

Furthermore another protection would be limiting the number of people that have access to the server (I imagine theteam of server administrators in hospital to be fairly small).

Lastly comunicating to customer that in SAAS solutons the added value is usually not in the code itself, but in the knowledge how to operate it, insturct the customer about usage, sacurity and functionality updates, knowledge of the codebase, knowledge how to integrate with other systems, connection to other necessary services (potentionaly ran on the provider infrastructure).
In case of carebot specifically a capability of developing newer and better models based on larger and better anotated datasets with faster inference speed?


## Technical meassures 
As the owner of the server has root access to the filesystem of the server running the container I think `prevent unauthorized copying` is impossible, at least for code. Furthermore having root access allows the operator to turn of any of those settings.

### System meassures 

*History logging* - should be implemented in any cases. Sessions of users than can be ocrrelated with the history command that tar-ed the container and `scp`d it to his/her server.

*Auditd*
Audit deamon which is proabably the most promissing of all the proposed solutions. Can watch files for modification. Can monitor system calls.

Disadvantages: Can be quite complex to setup (and maintain).


### Protection on the container level 

*Shell-less* containers (distorless) add another layer of obfuscation (if cusotomer cant "shell in" to the container it will be harder to retrieve filesystem contents and runtime variables).

Disadvantage: Debugging production during runtime is harder. 

### Software level protection

*Obfuscation tools* "ensures" even when access to containers filesystem the atacker will not be able to read the obfuscated code. He is then stuck with unupdatable, unforkable and old version of code. 

Disadvantage: Debugging production during runtime is harder. 


## Summary
Well writen licence agreemen and serious partnerships are above all technical meassures. 