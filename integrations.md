# Integration and Communication Diagram

Create a diagram illustrating the communication flow between a Picture Archiving and Communication System (PACS) and a Dockerized on-premise application using the DIMSE service, as outlined in the DICOM standard. Accompany your diagram with a comparative analysis of the benefits and limitations of using DIMSE versus DICOMweb, with a focus on scenarios applicable to a health tech environment. Include considerations for performance, compatibility, and security.

## Useful links
DIMSE documentation - https://www.dicomstandard.org/standards/view/message-exchange
DICOMweb documentation - https://www.dicomstandard.org/standards/view/web-services

## Disclaimer 
This question, especially the part comparing SE and Web is based mostly on assumptions. Due to the writers limited knowledge of the topic is is expected to be reviewed by the reader and it should be taken with a grain of salt.


## Diagram
```
 APP                            PACS           
  │         A-ASSOCIATE          │             
  ├──────────────────────────────►             
  │         A-ASSOCIATE          │             
  │◄─────────────────────────────┤             
  │                              │             
  │                              │             
  │        C-GET/MOVE/FIND       │ Probably GET
  ├─────────────────────────────►│            
  │              DATA            │             
  │◄─────────────────────────────┤             
P │                              │             
R │                              │             
O │                              │             
C │                              │             
E │                              │             
S │                              │             
S │                              │             
  ▼                              │             
  │                              │             
  │           C-STORE            │             
  ├─────────────────────────────►│             
  │                              │             
  │                              │             
  │                              │             
  │                              │             
  │                              │             
  │           RELEASE            │             
  ├─────────────────────────────►│             
  │                              │             
```
`N-` (non blocking} variants can be also used (that would make the communication flow ) 


## Comparison between DIMSE and DICOMweb
*performance* - utilizing HTTP protocol will surely bring SOME overhead, on the other hand it will probably "force" nonblocking operation by default. Assuming the overhead to be marginal compared to process time.

*compatibility* - DIMSE seems to be much older than DimcomWEB.
During the interview it was mentioned that carebot integrates with really old software, this would favor DIMSE. 

*security* - communication within private network should be safe either way, if comunication over public internet (not a good practice) IPsec or similar should be used.

## Glossary
AE - application entity 

SCP - provider (producer) (here PACS)

SCU - user (consumer) (here application))

SOP - (?)  type of operation being performed + format and structure of the medical data