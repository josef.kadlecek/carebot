## Application Monitoring

List and describe the key metrics and logs you would monitor for an application deployed both on-premise and in the cloud. Discuss any differences in your monitoring strategy between the two environments and the tools you would employ to ensure effective observability.

# Tech stack 
Grafana, Prometheus, Loki, Alertmanager

## Grafana
Visualization tool that supports multiple datasources. Offers their own ruler (see rulers in loki and prometheus sections), supports a lot of comunity built dashboards (for example node exporter). 

## Prometheus 
Prometheus is tool for metrics collection. Metrics consists of labels and values. Only labels are indexed. 

A lot of commonly used tools already support prometheus based metrics (usually on /metrics endpoint). 
For example in Mysql there is metric for size of each database table (a lot of metrics), most webservers provide prometheus metrics (counter of 5xx/4xx/2xx requests, average request duration...).
Writing custom prometheus exporter is fairly easy and there is a library for every reasonable language (developers can decide which metric they find usefull - number of processed images, average processing time...) and create their dashboard for visualization (graf of processd images for each day for the past month, average processing time...).

Prometheus ruler can monitor if the average_processing_time did not increase after the last release or if there is expected number of requests. 

### Node exporter 
Node exporter is important to mention for monitoring (not only) customer  VMs. 
By deploying single binary and adding node exporter dashboard to grafana, we gain insight into resource utilization of the node (to answer questions like: do we have enough disk space, iops, ram, cpu power...) furthermore we can detect potential bottleneck and we know if something is going south (no more free disk space, node starts swapping, load average is 20x CPU count...). By utilizing ruler we cen get alerts on those events. 


## Logs collection, filtering and storage
Loki in combaination with Promtail is easy yet powerfull tool to collect (not only) container logs. When setup correctly, promatial can extract `lables` from docker engine (image name, container id, customer name, envrionment name...) as well as the logs itself. 
Loki has powerful "querry language" LogQL which is capable of filtering logs based on lables as well as contents. 
Loki has good integration with grafana (same company) and supports writing "loki rules" (alert me when there are more than 10 errors per hour, alert me when the logs contain trace back, alert me when there is a sudden spike in warnings...). Those alerts can be distributed to Alertmanager. 

## Alertmanager
The sole role of alertmanager is to recieve "alerts" from different tools (rulers as described above) and route them (write them to slack/telegram chanell, write an email, for the most serious send SMS to dev/ops...).

## Differences between on premise and self managed

Mostly deppens of reachability of customer infrastructure and resources customer is capable to provide.

There are several levels of federation that one can consider (for example one can have one grafana per customer). 

From my experience its better to have single grafana and alertmanager (in the cloud env) which will then querry Prometheis / Lokis  in the customers infra.
One can also have single Prometheus and Loki running in his infrastructure (fewer tools to maintain), but the downtime of bridge between cloud and customer infra can cause gaps in metrics/logs collection.

When loki and prometheus are running on customers infra, we dont waste our resources, furthermore bandwith is only utilized for querries/logs that we are actually interested in.  

When something goes terribly wrong (bridge between infras burn down, our grafana stops working...), as long as we can tunnell some ports to customers server, we can access the metrics/logging tools. 

The biggest advantage of recomended tools is they are well suited for both cloud and on premise infrstructure, they are defacto standard in current container observability tools and they they dont cause vendor lock in. Lastly they are open source and bring all the (dis)advantages of oupensource tools.
