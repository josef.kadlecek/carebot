# Design a CI/CD Pipeline


## Task
Given a multi-service Docker application, outline the design of a CI/CD pipeline for deployment to a Kubernetes cluster. Please include your choice of tools and services that you would use for this process, along with a brief explanation of each choice and how they interconnect within the pipeline.

## High level overview 
Separate repository for app(s) code and configuration. 
Application pipeline performs testing, building, validation and update of the configuration.
The config repo is pulled by Argo CD which ensures that the config matches the workload state.


## Tooling

The pipeline tool "does not matter" (feel free to skip to TLDR section)

Based on personall experience I prefer Gitlab CI as I find it the most readable, yet not lacking powers.
After quick glance at azure [apipleines example](https://github.com/microsoft/azure-pipelines-yaml/blob/master/templates/), they follow the same principle as Gitlab ones, `task` is `job` and everything is yaml and every `task` is "just a bash script".

Noteworthy alternatives I have experience with:
 * GithubActions (personal take: pretty decent alternative to gitlab ci)/
 * Jenkins (personal take: extremly powerfull, tons of integrations, easy to get lost in, complex setup, not KISS, nobody likes groovy..., )

*TLDR*: If there is tooling that company is happy with, keep using it. 
If there is a reason to migrate, I recomend Gtilab.

Apart from the pipeline tool itself I recomend using ArgoCD (will be further described below).

## App repo - the 'CI'

The app repository contains pipeline with validation, build, test, and update confiig steps. 

* Validation - mainly static code analysis - pylint, mypy if types are used, and any others that developers find fitting https://blog.codacy.com/python-static-analysis-tools .
* Build - usually invokes just docker build and upload to registry of choice.
* Test - runs unit / integration / regression / performance and any other tests that the devs write. 
Appart from unit test this can be highly dependent on a time budget as setting up "complete" test environment can be quite complex for large amounts of distributed servicies.
* Update step - updates or creates MR in the config repo

## Config repo - the 'CD'

First lets dicrabe what Argo CD is and does. 

Its a tool that runs in k8s and PULLS(1) the config yamls (or helm chart) from git ("gitops principle"). Furthermore Argo *monitors* the current state of deployment (pipeline usually puts the stuff to production and doesnt care. In the best case scenario it waits for healtchceck to pass). By monitoring manual and unexpected changes are deteced, the config repo is the "single source of truth", offers easy* rollback options (can revert to last working version). Lastly we have trackable changes with infrastructure as a code (we can track which version was deployed by whom and when).

Different ENVs (dev / stage / prod ...) can be handled by different branches (easy), or tools like `Kustomize` can be used (creating hierarchy of yaml files that can be merged / left out ).


### 1) The main benefit of pull based architecture 
Lets compare it to "push" based - the pipeline tool needs to ssh to server, copy files, runing deploy script... and so on. This approach requires us to configure access to k8s and the cloud platform  (which can be a security risk).
Furthermore in case of multiple clusters (prehaps single node customer clusters running something like microk8s ?) the configuration needs to be set and maintained for each and every cluster (with argo cd the CLUSTERS only need to be able to pool the git config repo).
Lastly human access - developers dont need access to production server(s), everyone can initiate changes (MR/PR) but only selected members can merge them (beneficial mainly for large teams, teams with a lot of interns, teams with a lot of external contractors (I believe some ISO will like this approach :) ).
